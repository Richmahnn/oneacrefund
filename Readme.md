# Blue-Yellow-Green Programming Problem

Please output a list of the first 100 numbers keeping in mind the following rules:
•	If a number is divisible by 3 should be replaced by the word "Blue".
•	If a number is divisible by 5 should be replaced by the word "Yellow".
•	If a number is divisible by both 3 and 5 should be replaced by the word "Green".
•	Every time a number is replaced by one of the above words, please provide both the count of how many times a number has been replaced by the colour and the running sum of numbers replaced for that colour


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Prerequisites

Any IDE that supports java development


```
Eclipse
```

### Installing

Here is how to run the program

```
- Open eclipse or any other java IDE
- Import the project into your IDE
- Locate the Main.java file under "src" folder of the project
- Right click on Main.java and click run as java application(For eclipse)
- Observe the console and see the output of the program
- Thats it.

```

## Built With

* [Eclipse](https://www.eclipse.org/downloads/packages/release/neon/3/eclipse-ide-java-ee-developers) - The IDE used