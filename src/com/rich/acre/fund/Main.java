package com.rich.acre.fund;

import java.util.ArrayList;

public class Main {

	private static ArrayList<String> first100numbers;
	private static int blueCount = 0;
	private static int yellowCount = 0;
	private static int greenCount = 0;
	
	private static int blueSum = 0;
	private static int yellowSum = 0;
	private static int greenSum = 0;
	
	public static void main(String[] args) {
		first100numbers = new ArrayList<String>();
	
		blueYellowGreen(100);
	}
	
	private static void blueYellowGreen(int total){
		for (int i=1; i<total; i++) {
			if (i%3 == 0) {
				blueCount = blueCount + 1;
				blueSum = blueSum + i;
				first100numbers.add("Blue(count="+blueCount+", sum="+blueSum+")");
			} else if (i%5 == 0) {
				yellowCount = yellowCount + 1;
				yellowSum = yellowSum + i;
				first100numbers.add("Yellow(count="+yellowCount+", sum="+yellowSum+")");
			} else {
				first100numbers.add(String.valueOf(i));
			}
			
			if (i%3 == 0 && i%5 == 0) {
				greenCount = greenCount + 1;
				greenSum = greenSum + i;
				first100numbers.add("Green(count="+greenCount+", sum="+greenSum+")");
			}
		}
		
		for (int n=0; n<first100numbers.size(); n++) {
			System.out.println(first100numbers.get(n));
		}
	}

}
